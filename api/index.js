import { feed } from './feed.json'

export const getPings = (userId) => {
  const pings = feed[userId].pings

  // fake async operation
  return new Promise((resolve) => resolve(pings))
}
