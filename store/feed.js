import { getPings } from '../api'

export const state = () => ({
  pings: [],
})

export const mutations = {
  setPings(state, pings) {
    state.pings = pings
  },
}

export const actions = {
  async fetchPings(context, profileId) {
    const pings = await getPings(profileId)
    console.log(pings)
    context.commit('setPings', pings)
  },
}
